"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @returns An express middleware to properly respond to ping-pong requests.
 * @author Johan Svensson
 */
exports.pong = function () { return function (req, res, next) {
    if (/\/ping\/?$/i.test(req.originalUrl)) {
        //  is a pong request!
        return handlePongRequest(req, res);
    }
    return next();
}; };
function handlePongRequest(req, res) {
    return res.status(200).end('PONG');
}
exports.handlePongRequest = handlePongRequest;
