import { Request, Response, NextFunction } from "express";
/**
 * @returns An express middleware to properly respond to ping-pong requests.
 * @author Johan Svensson
 */
export declare const pong: () => (req: Request, res: Response, next: NextFunction) => void;
export declare function handlePongRequest(req: Request, res: Response): void;
