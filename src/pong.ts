import { Request, Response, NextFunction } from "express";

/**
 * @returns An express middleware to properly respond to ping-pong requests.
 * @author Johan Svensson
 */
export const pong = () => (req: Request, res: Response, next: NextFunction) => {
  if (/\/ping\/?$/i.test(req.originalUrl)) {
    //  is a pong request!
    return handlePongRequest(req, res);
  }

  return next();
}

export function handlePongRequest(req: Request, res: Response) {
  return res.status(200).end('PONG');
}