# Pong module
##  Purpose
This module allows for responding to a ping request.

##  How to use
Given an `express` app named `app`, invoke `app.use(pong())` to register the pong handler middleware.